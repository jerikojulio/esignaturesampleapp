//
//  ViewController03.swift
//  ICESignatureSample
//
//  Created by Jeriko on 3/7/17.
//  Copyright © 2017 Jeriko. All rights reserved.
//

import UIKit
import ICESignature
import LayoutConverter

class ViewController03: UIViewController {
    
    var docController: UIDocumentInteractionController?
    
    @IBOutlet weak var imageSign: UIImageView!
    
    @IBOutlet weak var userDesignated: UITextView!
    
    @IBOutlet weak var signArea: UIImageView!
    
    var drawAreaOriginPoint = CGPoint()
    var drawAreaRekt = CGRect()
    var drawArea = ICESignature()
    var layoutConverter: LayoutConverter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Clickable Area"
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap))
        self.imageSign.addGestureRecognizer(tap)
        self.imageSign.isUserInteractionEnabled = true
        
    }
    
    override func viewDidLayoutSubviews() {
        self.userDesignated.setContentOffset(CGPoint.zero, animated: false)
        self.userDesignated.scrollRangeToVisible(NSMakeRange(0, 0))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        guard let signImage = globals.signatureImage else {
            return
        }
        //        drawArea.backgroundColor = UIColor(patternImage: signImage)
        //        drawArea.layer.contents = signImage
        imageSign.image = signImage
        //        imageSign.layer.backgroundColor = UIColor(patternImage: signImage).cgColor
        
    }
    
    func handleTap(recognizer: UIGestureRecognizer){
        
        let tappedLocation = recognizer.location(in: userDesignated)
        
        drawAreaOriginPoint = tappedLocation
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let secondViewController = storyBoard.instantiateViewController(withIdentifier: "signatureEditor") as! SignatureEditingViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
        
        globals.signatureClass = drawArea
        drawArea.frame = imageSign.frame
        drawAreaRekt = drawArea.frame
    }
    
    @IBAction func convertToImage(_ sender: UIButton) {
        
        layoutConverter = LayoutConverter(pageSize: self.view.frame.size)
        layoutConverter?.addView(view: self.view)
        layoutConverter?.saveViewToGallery()
        
        let alert = UIAlertController(title: "Success!", message: "Layout Saved to Photos", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func convertToPDF(_ sender: UIButton) {
        
        layoutConverter = LayoutConverter(pageSize: self.view.frame.size)
        
        layoutConverter?.addView(view: self.view)
        
        let pdfData = layoutConverter?.generatePDFdata()
        
        do {
            let fileURL = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent("research.pdf")
            
            print("Document Path: \(fileURL)")
            
            try pdfData?.write(to: fileURL, options: .atomic)
            
            self.docController = UIDocumentInteractionController(url: fileURL)
            
            self.docController?.presentOpenInMenu(from: CGRect.zero, in: self.view, animated: true)
            
        } catch let error {
            print(error.localizedDescription)
        }
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showNextVC" {
            globals.signatureImage = nil
        }
    }
    
}
