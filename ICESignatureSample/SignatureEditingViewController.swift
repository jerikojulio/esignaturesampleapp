//
//  SignatureEditingViewController.swift
//  ICESignatureSample
//
//  Created by Jeriko on 2/23/17.
//  Copyright © 2017 Jeriko. All rights reserved.
//

import UIKit
import ICESignature

class SignatureEditingViewController: UIViewController {
    
    @IBOutlet weak var drawingArea: ICESignature!
    
    @IBAction func saveSignature(_ sender: UIButton) {
        
        let size = drawingArea.frame.size
        
        UIGraphicsBeginImageContext(size)
        let context = UIGraphicsGetCurrentContext()
        drawingArea.layer.draw(in: context!)
        let screenshot = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        let newSize = globals.signatureClass.frame.size

        UIGraphicsBeginImageContextWithOptions(newSize, false, 0)
        var image = screenshot
        image.draw(in: globals.signatureClass.bounds)
        image = UIGraphicsGetImageFromCurrentImageContext()!;
        UIGraphicsEndImageContext()
        
        globals.signatureImage = image
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clear(_ sender: UIButton) {
        drawingArea.clearLines()
    }
    
    @IBAction func begin(_ sender: UIButton) {
        drawingArea.isEditing = true
    }
    
    @IBAction func stop(_ sender: UIButton) {
        drawingArea.isEditing = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
