//
//  ViewController01.swift
//  ICESignatureSample
//
//  Created by Jeriko on 2/23/17.
//  Copyright © 2017 Jeriko. All rights reserved.
//

import UIKit
import ICESignature

class ViewController01: UIViewController {
    
    @IBOutlet weak var drawingArea: ICESignature!
    
    @IBAction func clear(_ sender: UIButton) {
        drawingArea.clearLines()
        drawingArea.backgroundColor = UIColor.clear
    }
    
    @IBAction func begin(_ sender: UIButton) {
        drawingArea.isEditing = true
    }
    
    @IBAction func stop(_ sender: UIButton) {
        drawingArea.isEditing = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.title = "New Area"
    }
    
    override func viewDidLayoutSubviews() {
        globals.signatureClass = drawingArea
    }
    
    override func viewDidAppear(_ animated: Bool) {
        guard let signatureImage = globals.signatureImage else {
            return
        }
        drawingArea.clearLines()
        drawingArea.backgroundColor = UIColor.clear
        drawingArea.backgroundColor = UIColor(patternImage: signatureImage)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
