//
//  ViewController02.swift
//  ICESignatureSample
//
//  Created by Jeriko on 2/23/17.
//  Copyright © 2017 Jeriko. All rights reserved.
//

import UIKit
import ICESignature

class ViewController02: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var drawScrollView: UIScrollView!
    var imageView = UIImageView()
    var parentSubview = UIView()
    var drawingArea = ICESignature()
    
    @IBAction func clear(_ sender: UIButton) {
        drawingArea.clearLines()
    }
    
    @IBAction func begin(_ sender: UIButton) {
        drawingArea.isEditing = true
        lockZoom()
    }
    
    @IBAction func stop(_ sender: UIButton) {
        unlockZoom()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        drawScrollView.delegate = self
        imageView.image = #imageLiteral(resourceName: "receipt.jpeg")
        drawScrollView.backgroundColor = UIColor.red
        setUp()
        
        self.title = "Inside Scrollview"
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    func setUp(){
        let size = imageView.image?.size
        
        parentSubview.frame = drawScrollView.bounds
        parentSubview.frame.size = size!
        
        imageView.frame = drawScrollView.bounds
        imageView.frame.size = size!
        
        rescaleImageToFitHolder()
        
        drawScrollView.contentSize.height = imageView.frame.height
        drawScrollView.contentSize.width = imageView.frame.width
        
        drawScrollView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        drawScrollView.addSubview(parentSubview)
        parentSubview.addSubview(imageView)
        
        let drawingAreaSize = CGSize(width: parentSubview.frame.width, height: parentSubview.frame.height * 0.3)
        let drawingAreaPosition = CGPoint(x: 0, y: parentSubview.frame.height * 0.7)
        let rekt = CGRect(origin: drawingAreaPosition, size: drawingAreaSize)
        
        drawingArea.frame = rekt
        drawingArea.backgroundColor = UIColor.clear
        
        parentSubview.addSubview(drawingArea)
        
        setZoomProperties()
    }
    
    func rescaleImageToFitHolder(){
        let imageViewBounds = imageView.bounds.size
        let mainScrollViewBounds = drawScrollView.bounds.size
        
        let mainViewfactor = max(mainScrollViewBounds.height, mainScrollViewBounds.width)
        let imageViewFactor = max(imageViewBounds.height, imageViewBounds.width)
        
        let imageViewWidthFactor = imageViewBounds.width / imageViewFactor
        let imageViewHeightFactor = imageViewBounds.height / imageViewFactor
        
        let mainViewWidthFactor = mainScrollViewBounds.width / mainViewfactor
        let mainViewHeightFactor = mainScrollViewBounds.height / mainViewfactor
        
        var newHeight = CGFloat()
        var newWidth = CGFloat()
        
        if (mainViewWidthFactor / imageViewWidthFactor) == 1 {
            newHeight = mainViewWidthFactor * imageViewBounds.width
            newWidth = imageViewBounds.width
        }
        
        if (mainViewHeightFactor / imageViewHeightFactor) == 1 {
            newHeight = imageViewBounds.height
            newWidth = mainViewWidthFactor * imageViewBounds.height
        }
        
        if (mainViewHeightFactor / imageViewWidthFactor) == 1 {
            newHeight = imageViewBounds.height
            newWidth = imageViewBounds.width * imageViewHeightFactor * mainViewWidthFactor
        }
        
        if (mainViewWidthFactor / imageViewHeightFactor) == 1 {
            newHeight = imageViewBounds.height * imageViewWidthFactor * mainViewHeightFactor
            newWidth = imageViewBounds.width
        }
        
        imageView.frame.size.height = newHeight
        imageView.frame.size.width = newWidth
        parentSubview.frame = imageView.frame
    }
    
    override func viewWillLayoutSubviews() {
        setZoomProperties()
    }
    
    public func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return parentSubview
    }
    
    func setZoomProperties() {
        let imageViewBounds = imageView.bounds.size
        let mainScrollViewBounds = drawScrollView.bounds.size
        
        let widthScale = mainScrollViewBounds.width / imageViewBounds.width
        let heightScale = mainScrollViewBounds.height / imageViewBounds.height
        
        drawScrollView.minimumZoomScale = max(widthScale,heightScale)
        drawScrollView.maximumZoomScale = 0.5
        
        drawScrollView.zoomScale = 0
    }
    
    func lockZoom(){
        drawScrollView.pinchGestureRecognizer?.isEnabled = false
        drawScrollView.panGestureRecognizer.isEnabled = false
    }
    
    func unlockZoom(){
        drawScrollView.pinchGestureRecognizer?.isEnabled = true
        drawScrollView.panGestureRecognizer.isEnabled = true
    }
}
