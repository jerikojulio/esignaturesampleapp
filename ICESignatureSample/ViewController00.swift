//
//  ViewController.swift
//  ICESignatureSample
//
//  Created by Jeriko on 2/22/17.
//  Copyright © 2017 Jeriko. All rights reserved.
//

import UIKit
import ICESignature

class ViewController00: UIViewController {
    
    @IBAction func shareThings(_ sender: UIButton) {
        self.navigationController?.present(activityVC, animated: true, completion: nil)
    }
    
    @IBOutlet weak var drawArea: ICESignature!
    
    let activityVC = UIActivityViewController(activityItems: ["test","www.gmail.com",#imageLiteral(resourceName: "receipt.jpg")], applicationActivities: nil)
    
    @IBAction func clear(_ sender: UIButton) {
        drawArea.clearLines()
    }
    
    @IBAction func beginEditing(_ sender: UIButton) {
        drawArea.isEditing = true
    }
    
    @IBAction func finishEditing(_ sender: UIButton) {
        drawArea.isEditing = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        drawArea.layer.borderWidth = 1
        drawArea.layer.borderColor = UIColor.black.cgColor
        drawArea.lineColor = UIColor.red
        //        drawArea.disablePenMode()
        
        /*
         let backgroundImage = UIImageView(frame: drawArea.frame)
         backgroundImage.image = #imageLiteral(resourceName: "receipt.jpeg")
         self.view.insertSubview(backgroundImage, at: 0)
         */
        
        self.title = "Standard"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        /*
         let textRect = CGRect(x: 100 , y: 100 , width: 100 , height: 100)
         
         let paragraphStyle:NSMutableParagraphStyle = NSMutableParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
         paragraphStyle.alignment = NSTextAlignment.left
         paragraphStyle.lineBreakMode = NSLineBreakMode.byWordWrapping
         
         let textColor = UIColor.black
         
         let font = UIFont(name: "Helvetica Bold", size: 14.0)
         
         let textFontAttributes = [
         NSFontAttributeName: font!,
         NSForegroundColorAttributeName: textColor,
         NSParagraphStyleAttributeName: paragraphStyle
         ]
         
         let text:NSString = "Hello World"
         
         text.draw(in: textRect, withAttributes: textFontAttributes)
         */
    }
    
}

