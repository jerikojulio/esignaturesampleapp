//
//  ViewController04.swift
//  ICESignatureSample
//
//  Created by Jeriko on 3/7/17.
//  Copyright © 2017 Jeriko. All rights reserved.
//

import UIKit
import ICESignature
import LayoutConverter

class ViewController04: UIViewController {
    
    var docController: UIDocumentInteractionController?
    
    var layoutConverter: LayoutConverter?
    
    @IBOutlet weak var signImage: UIImageView!
    
    var drawArea = ICESignature()
    var drawAreaOriginPoint = CGPoint()
    var drawAreaRekt = CGRect()
    
//    @IBOutlet weak var drawScrollView: UIScrollView!
    @IBOutlet weak var imageContainer: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
//        drawScrollView.delegate = self
        
        imageContainer.image = #imageLiteral(resourceName: "exampleReceipt")
        imageContainer.frame.size = imageContainer.image!.size
//        
//        drawScrollView.backgroundColor = UIColor.black
//        drawScrollView.contentSize = imageContainer.bounds.size
//        drawScrollView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//        drawScrollView.addSubview(imageContainer)
        
//        setZoomProperties()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap))
        self.view.addGestureRecognizer(tap)
        self.view.isUserInteractionEnabled = true
        imageContainer.isUserInteractionEnabled = false
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        
        guard let signImage02 = globals.signatureImage else {
            return
        }
        
        drawArea.backgroundColor = UIColor(patternImage: signImage02)
        self.signImage.image = signImage02
        self.signImage.frame = drawArea.frame
//        imageContainer.addSubview(signImage)
    }
    
    func handleTap(recognizer: UIGestureRecognizer){
        
        let tappedLocation = recognizer.location(in: imageContainer)
        
        drawAreaOriginPoint = tappedLocation
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let secondViewController = storyBoard.instantiateViewController(withIdentifier: "signatureEditor") as! SignatureEditingViewController
        //        self.present(secondViewController, animated:true, completion:nil)
        self.navigationController?.pushViewController(secondViewController, animated: true)
        drawAreaRekt = CGRect(x: drawAreaOriginPoint.x - 55, y: drawAreaOriginPoint.y + 55, width: 150, height: 150)
        drawArea.frame = drawAreaRekt
        globals.signatureClass = drawArea
    }
    @IBAction func convertToImage(_ sender: UIButton) {
        
        layoutConverter = LayoutConverter(pageSize: self.view.frame.size)
        
        layoutConverter?.addView(view: self.view)
        
        layoutConverter?.saveViewToGallery()
        
        let alert = UIAlertController(title: "Success!", message: "Layout Saved to Photos", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func convertToPDF(_ sender: UIButton) {
        
        layoutConverter = LayoutConverter(pageSize: self.view.frame.size)
        
        layoutConverter?.addView(view: self.view)
        
        let pdfData = layoutConverter?.generatePDFdata()
        
        do {
            let fileURL = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent("research.pdf")
            
            print("Document Path: \(fileURL)")
            
            try pdfData?.write(to: fileURL, options: .atomic)
            
            self.docController = UIDocumentInteractionController(url: fileURL)
            
            self.docController?.presentOpenInMenu(from: CGRect.zero, in: self.view, animated: true)
            
        } catch let error {
            print(error.localizedDescription)
        }
    }
}

extension ViewController04: UIScrollViewDelegate {
//    func setZoomProperties() {
//        let imageViewBounds = imageContainer.bounds.size
//        let mainScrollViewBounds = drawScrollView.bounds.size
//        
//        let widthScale = mainScrollViewBounds.width / imageViewBounds.width
//        let heightScale = mainScrollViewBounds.height / imageViewBounds.height
//        
//        drawScrollView.minimumZoomScale = max(widthScale,heightScale)
//        drawScrollView.maximumZoomScale = 0.5
//        
//        drawScrollView.zoomScale = 0
//    }
//    
//    public func viewForZooming(in scrollView: UIScrollView) -> UIView? {
//        return imageContainer
//    }
}
