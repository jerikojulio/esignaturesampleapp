//
//  Singletons.swift
//  ICESignatureSample
//
//  Created by Jeriko on 2/23/17.
//  Copyright © 2017 Jeriko. All rights reserved.
//

import UIKit
import ICESignature

struct globals {
    static var signatureClass: ICESignature!
    static var signatureImage: UIImage!
}
